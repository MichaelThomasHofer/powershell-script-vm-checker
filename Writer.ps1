#Author: Michaël Hofer
#Creation Date: 03.02.2020
#Last Update: 13.02.2020
#Description: This scrit get all the Expected & actual VM datas from his parent (Main)
#Then compare the actual & expected data and finally write the result in the output file

param(
    [string]$expectedVMName,
    [string]$expectedVMDNS,
    [string]$expectedVMDHCP,
    [string]$expectedADDS,
    [string]$VMNAme,
    [string]$VMDNS,
    [string]$VMDHCP,
    [string]$VMADDS,
    #OFS is the parameter for \n (Line break)
    [string]$OFS = "`r`n"
)

$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
cd $ScriptDir

#Get OS Data Of the machine	
$OSData = Get-WmiObject Win32_OperatingSystem
$MachineOSFullName = $OSData.Name.ToString()
$MachineOsName = $MachineOSFullName -split "C:"

#Delete output.txt if it already exist
try
{
    $outputFile = (Get-Location).Path + '\output.txt'
    Remove-Item $outputFile
}catch
{
  Write-Host "The Output.txt file has been created"
}
        
#Create à new output.txt file
New-Item $outputFile -ItemType file
    
#Try to write the result in output.txt file
try
{    
    $writer = [system.IO.Streamwriter] $outputFile
               
    if($MachineOsName -match "Server")
    {
       
    }
    else
    {
        $writer.WriteLine("The machine OS isnt a Windows Server OS" + $OFS)
    }

    #If the actual VM name is equal to the expected VM Name
    if($expectedVMName -eq $VMNAme)
    {
        $writer.writeline('The Expected Name is equal to this VM Name (' + $VMNAme + ")" + $OFS)
    }else{
        $writer.writeline('The Expected Name is not equal to this VM Name' + $OFS + 'actual :   ' + $VMNAme + $OFS + 'Expected : ' + $expectedVMName + $OFS)
    }
        
    #If the actual DNS informations are equals to the expected DNS informations
    if($expectedVMDNS -eq $VMDNS)
    {
        $writer.writeline('The Expected DNS is equal to this VM DNS (' + $VMDNS + ")" + $OFS)
    }else{
        $writer.writeline('The Expected DNS is not equal to this VM DNS' + $OFS + 'actual :   ' + $VMDNS + $OFS + 'Expected : ' + $expectedVMDNS + $OFS) 
    }
        
    #If the actual DHCP informations are equals to the expected DHCP informations       
    if($expectedVMDHCP -eq $VMDHCP)
    {
        $writer.writeline('The Expected DHCP is equal to this VM DHCP (' + $VMDHCP + ")" + $OFS)
    }else{
        $writer.writeline('The Expected DHCP is not equal to this VM DHCP' + $OFS + 'actual :   ' + $VMDHCP + $OFS + 'Expected : ' + $expectedVMDHCP + $OFS) 
    }
        
    #If the actual AD DS informations are equals the the expected DNS informations        
    if($expectedADDS -eq $VMADDS)
    {
        $writer.writeline('The Expected AD DS is equal to this VM AD DS (' + $VMADDS + ")" + $OFS)
    }else{
        $writer.writeline('The Expected AD DS is not equal to this VM AD DS' + $OFS + 'actual :   ' + $VMADDS + $OFS + 'Expected : ' + $expectedADDS + $OFS + $OFS) 
    }
                
    #Close the output.txt file
    $writer.close()
               
}finally
{

}