#Author: Michaël Hofer
#Creation Date: 05.02.2020
#Last Update: 13.02.2020
#Description: This Script is designed to check if the DNS,DHCP,AD DS exist on the VM and send the result to his parent (Main)
param
(
    $file,
    $parentvariable
    
)
# init the attributs of the script
$VMFeatureToSend = new-object bool[] 4
#DHCP Value
$VMFeatureToSend[0] = $false

#DNS Value
$VMFeatureToSend[1] = $false

#AD DS Value
$VMFeatureToSend[2] = $false

#If machine isn't a windows server Value
$VMFeatureToSend[3] = $false

try
{   
    $Features = Get-WindowsFeature -ComputerName $env:computername | Where Installed
    
    for($i = 0; $i -le 3; $i ++ )
    {
        if($Features[$i].Name -like "DHCP")
        {
            $VMFeatureToSend[0] = $true
        }

        if($Features[$i].Name -like "DNS")
        {
            $VMFeatureToSend[1] = $true
        }

        if($Features[$i].Name -like "AD-Domain-Services")
        {
            $VMFeatureToSend[2] = $true
        } 
    }    
}
catch
{
$VMFeatureToSend[3] = $true

}
#Send if the DNS, DHCP or AD DS exist to the Main script
Set-Variable -Name $parentvariable -Value $VMFeatureToSend -Scope 1