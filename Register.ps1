#Author: Michaël Hofer
#Creation Date: 04.02.2020
#Last Update: 13.02.2020
#Description: This Script is designed to get the VM Name and send it to his parent (Main)

param
(
    $file,
    $parentvariable
)

$name = $env:computername
        
#Send the VM display name to the main script (Main)
Set-Variable -Name $parentvariable -Value $name -Scope 1