#Author: Michaël Hofer
#Creation Date: 03.02.2020
#Last Update: 13.02.2020
#Description: This Script is the main script of the VM checker project. It call the other scripts and get the result from them.
#And send all the informations to Writer after collect them all.
param
(
    [string]$expectedVMName,
    [string]$expectedVMDNS,
    [string]$expectedVMDHCP,
    [string]$expectedADDS,
    [string]$VMActualNnnname = "DefaultName" 
)
#Attributes init
$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
$flags = new-object bool[] 3
$flags[0] = $false
$flags[1] = $false
$flags[2] = $false

#Ask expectedVMName to the user (any string ok !)
$expectedVMName = Read-Host -Prompt 'Enter the Expected Machine Name'
Write-Host ""

#Ask expectedVMDNS to the user (will only validate when = true or false)
while($flags[0] -eq $false)
{
    if($expectedVMDNS -ne $true)
    {
        if($expectedVMDNS -ne $false)
        {
        $expectedVMDNS = Read-Host -Prompt 'Enter the expected DNS value (true ou false)'
        }else{
        $flags[0] = $true
        Write-Host ""
        }
    }else
    {
    $flags[0] = $true
    Write-Host ""
    }
}

#Ask expectedVMDHCP to the user (will only validate when = true or false)
while($flags[1] -eq $false)
{
    if($expectedVMDHCP -ne $true)
    {
        if($expectedVMDHCP -ne $false)
        {
        $expectedVMDHCP = Read-Host -Prompt 'Enter the expected DHCP value (true ou false)'
        }else{
        $flags[1] = $true
        Write-Host ""
        }
    }else
    {
    $flags[1] = $true
    Write-Host ""
    }
}

#Ask expectedADDS to the user (will only validate when = true or false)
while($flags[2] -eq $false)
{
    if($expectedADDS -ne $true)
    {
        if($expectedADDS -ne $false)
        {
        $expectedADDS = Read-Host -Prompt "Enter the expected AD-DS value (true ou false)"
        }else{
        $flags[2] = $true
        Write-Host ""
        }
    }else
    {
    $flags[2] = $true
    Write-Host ""
    }
}

#Create VMFeature (For the Features Return)
$VMFeatures = new-object bool[] 3
   

    try{
        #Call Features.ps1 to look for the Vms actual parameters   
        &"$ScriptDir\Features.ps1" $fileCommon1 "VMFeatures"
        if ($VMFeatures) {
            $VMactualDHCP  = $VMFeatures[0]
            $VMactualDNS = $VMFeatures[1]
            $VMactualADDS = $VMFeatures[2]        
        }
    }
    catch
    {
        Write-Host "An error has ben detected in Features" -ForegroundColor Red
    }

       try{         
        #Call Register.ps1 script to look for the VM actual name    
        &"$ScriptDir\Register.ps1" $fileCommon "ActualVMNAme"
        if ($ActualVMNAme) {
        }
    }
    catch
    {
        Write-Host "An error has been detected on Register" -ForegroundColor Red
    }

    try
    {
        #Call the writer.ps1 script, send all the VM actual datas and the expected results to writer
        $command =“$ScriptDir\Writer.ps1 –VMNAme $ActualVMNAme -VMDNS $VMactualDNS -VMDHCP $VMactualDHCP -VMADDS $VMactualADDS -expectedVMName $expectedVMName -expectedVMDNS $expectedVMDNS -expectedVMDHCP $expectedVMDHCP -expectedADDS $expectedADDS"
        Invoke-Expression $command
    }
    catch
    {
         Write-Host "An error has been detected in Writer" -ForegroundColor Red
    }